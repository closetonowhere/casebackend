<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php
echo GridView::widget([
  'dataProvider'=>$dataProvider,
  'columns' => [
  	'id',
            'order',
            'date_of_order',
  ]
]); 
?>

