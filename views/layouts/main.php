<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap">

        <div class="container">


            <?php 
            if(!Yii::$app->user->isGuest){
                  $links_arr = Yii::$app->user->identity->role === 'admin'?[
                ['label' => 'Данные клиентов', 'url' => ['/admin']],
                ['label' => 'Ручная оплата', 'url' => ['/admin/manualpayments/index']],
                ['label' => 'Заказы на устройства', 'url' => ['/orders']],
                ['label' => 'Яндекс-касса оплаты', 'url' => ['/yandexpayments']],
                ['label' => 'Предупредить клиентов', 'url' => ['/notice']],
                ['label' => 'Выйти', 'url' => ['/site/logout']],

            ] : [
                ['label' => 'Изменить пароль', 'url' => ['/forgotpass']],
                    ['label' => 'Выйти', 'url' => ['/site/logout']],
            ] ;
            }
            else{
                $links_arr = [
                ];
            }
         

            NavBar::begin(['brandLabel' => 'Admin page']);
            echo Nav::widget([
                'items' => $links_arr,
                'options' => ['class' => 'navbar-nav'],
            ]);
            NavBar::end();
            ?>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>

            <?= $content ?>
        </div>
    </div>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>