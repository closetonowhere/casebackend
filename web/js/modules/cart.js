'use strict';

// Модуль корзины
var cart = (function($) {




 var ui = {
        $orderForm: $('#order-form'),
        $messageCart: $('#order-message'),
        $orderBtn: $('#order-btn'),
        $alertValidation: $('#alert-validation'),
        $alertOrderDone: $('#alert-order-done'),
        $orderMessageTemplate: $('#order-message-template'),
        $fullSumma: $('#full-summa'),
        $delivery: {
            type: $('#delivery-type'),
            summa: $('#delivery-summa'),
            btn: $('.js-delivery-type'),
            alert: $('#alert-delivery')
        }
    };



  


    var cartData,
        opts = {};

    // Инициализация модуля
    function init(options) {
        _initOptions(options);
        updateData();
        if (opts.renderCartOnInit) {
            renderCart();
        }
        if (opts.renderMenuCartOnInit) {
            renderMenuCart();
        }
        _bindHandlers();
    }

    // Инициализируем настройки
    function _initOptions(options) {
        var defaultOptions = {
            renderCartOnInit: true,
            renderMenuCartOnInit: true,
            elAddToCart: '.js-add-to-cart',
			$orderBtn: $('#order-btn'),
            attrId: 'data-id',
			attrPhoto: 'data-photo',
            attrName: 'data-name',
            attrPrice: 'data-price',
            attrDelta: 'data-delta',
		    elCart: '#cart',
            elTotalCartCount: '#total-cart-count',
            elTotalCartSumma: '#total-cart-summa',
            elCartItem: '.js-cart-item',
            elCartCount: '.js-count',
            elCartSumma: '.js-summa',
            elChangeCount: '.js-change-count',
			btn: $('.js-delivery-type'),
            elRemoveFromCart: '.js-remove-from-cart'
        };
        _.defaults(options || {}, defaultOptions);
        opts = _.clone(options);
    }

    // Навешивам события
    function _bindHandlers() {
        _onClickAddBtn();
        _onClickChangeCountInCart();
        _onClickRemoveFromCart();
		  _checkCart();
    }


    // В случае пустой корзины отключаем кнопку Отправки заказа
    function _checkCart() {
        if (cart.getCountAll() === 0) {
            ui.$orderBtn.attr('disabled', 'disabled');
        }
    }

    // Получаем данные
    function updateData() {
        cartData = JSON.parse(localStorage.getItem('cart')) || [];
        return cartData;
    }

    // Возвращаем данные
    function getData() {
        return cartData;
    }

    // Сохраняем данные в localStorage
    function saveData() {
        localStorage.setItem('cart', JSON.stringify(cartData));
        return cartData;
    }

    // Очищаем данные
    function clearData() {
        cartData = [];
        saveData();
        return cartData;
    }

    // Поиск объекта в коллекции cartData по id
    function getById(id) {
        return _.findWhere(cartData, {id: id});
    }

    // Добавление товара в коллекцию
    function add(item) {
        var Item;
        updateData();
        Item = getById(item.id);
        if(!Item) {
            cartData.push(item);
        } else {
			if(Item.count==9)//ограничение на 9 товаров(можно вывести предупрждение об этом)
			{
				//тут может быть предупрждение
			}
			else
			{
            Item.count = Item.count + item.count;
			}
        }
        saveData();
        return item;
    }

    // Удаление товара из коллекции
    function remove(id) {
        updateData();
        cartData = _.reject(cartData, function(item) {
            return item.id === id;
        });
        saveData();
		_checkCart();
        return cartData;
    }

    // Изменение количества товара в коллекции
    function changeCount(id, delta) {
        var item;
        updateData();
        item = getById(id);
		  item.count = item.count + delta;
		  if(item) {
		if (item.count>9)
		{
			  item.count = item.count-delta;
            if (item.count < 1) {
                remove(id);
            }
            saveData();
			 return _.findWhere(cartData, {id: id}) || {};
        }
       
		else{
        
          
            if (item.count < 1) {
                remove(id);
            }
            saveData();
        
        return _.findWhere(cartData, {id: id}) || {};
		}
		}
    }

    // Возвращаем количество товаров (количество видов товаров в корзине)
    function getCount() {
        return _.size(cartData);
    }

    // Возвращаем общее количество товаров 
    function getCountAll() {
        return _.reduce(cartData, function(sum, item) {return sum + item.count}, 0);
    }

    // Возвращаем общую сумму
    function getSumma() {
        return _.reduce(cartData, function(sum, item) {return sum + item.count * item.price}, 0);
    }






    // Рендерим корзину
    function renderCart() {
        var template = _.template($('#cart-template').html()),
            data = {
                goods: cartData
            };
        $(opts.elCart).html(template(data));
        renderTotalCartSumma();
    }

    // Рендерим количество товаров в меню
    function renderMenuCart() {
        var countAll = getCountAll();
        $(opts.elTotalCartCount).html(countAll !== 0 ? countAll : '');
    }

    // Рендерим общую сумму товаров
    function renderTotalCartSumma() {
        $(opts.elTotalCartSumma).html(getSumma());            
    }



    // Поиск продукта в корзине по id
    function findCartElemById(id) {
        return $(opts.elCartItem + '[' + opts.attrId + '="'+id+'"]');
    }

    // Добавление в корзину
    function _onClickAddBtn() {
        $('body').on('click', opts.elAddToCart, function(e) {
            var $this = $(this);
		 
			
			
			add({
                id: +$this.attr(opts.attrId),
                name: $this.attr(opts.attrName),
                price: +$this.attr(opts.attrPrice),
				photo: $this.attr(opts.attrPhoto),
                count: 1
            });      
		    
            renderMenuCart();
           // можно без уведомления пользователя о добавлении товара
        });
    }

    // Меняем количество товаров в корзине
    function _onClickChangeCountInCart() {
        $('body').on('click', opts.elChangeCount, function(e) {
		
            var $this = $(this),
                id = +$this.attr(opts.attrId),
                delta = +$this.attr(opts.attrDelta),
                $cartElem = findCartElemById(id),
                cartItem = changeCount(id, delta);
					if (cartItem.count>=10)
					{
						
					}
					else{
            if (cartItem.count) {
                $cartElem.find(opts.elCartCount).html(cartItem.count);
                $cartElem.find(opts.elCartSumma).html(cartItem.count * cartItem.price);
            } else {
                $cartElem.remove();
            }
					}
            renderMenuCart();
            renderTotalCartSumma();
        });
    }

    // Удаляем товар из корзине
    function _onClickRemoveFromCart() {
        $('body').on('click', opts.elRemoveFromCart, function(e) {
            if(!confirm('Удалить товар из корзины?')) return false;
            var $this = $(this),
                id = +$this.attr(opts.attrId),
                $cartElem = findCartElemById(id);
            remove(id);
            $cartElem.remove();
            renderMenuCart();
            renderTotalCartSumma();
			// блокируем кнопку
			  if (cart.getCountAll() === 0) {
            ui.$orderBtn.attr('disabled', 'disabled');
        }
        });
		 
    }



    // Экспортируем наружу
    return {
        init: init,
        update: updateData,
        getData: getData,
        save: saveData,
        clearData: clearData,
        getById: getById,
        add: add,
        remove: remove,
        changeCount: changeCount,
        getCount: getCount,
        getCountAll: getCountAll,
        getSumma: getSumma
    }

})(jQuery);