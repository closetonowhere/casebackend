<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Orders;
use yii\data\ActiveDataProvider;



class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

//Получить данные из приложения
    public function actionReceive()
        {
            $order = new Orders();
            if(!empty(Yii::$app->request->post('orders'))){
                if($order->load(Yii::$app->request->post('orders')) and $order->validate()){
                    $order->date_of_order = date_format(new DateTime(), 'Y-m-d H:i:s');
                    $order->save();
                }
                return \Yii::createObject([
                                    'class' => 'yii\web\Response',
                                   'format' => \yii\web\Response::FORMAT_JSON,
                                   'data' => [
                                      'data' => 'Done',
                                  ],
                              ]);
            }
        }

    public function actionIndex()
    {
        $query_clients = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query_clients,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
