<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\Details;

class Orders extends ActiveRecord
{

    public static function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return [
            [[
            'operator_id',
            'date_of_order',
            ], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
              'id' => 'Номер заказа',
            'operator_id' => 'Номер оператора',
            'order' => 'Заказ',
            'date_of_order' => 'Дата заказа',
        ];
    }

    public function getDetails()
    {
        return $this->hasMany(Details::className(), ['order_id' => 'id']);
    }
}
