<?php

namespace app\models\search;

use app\models\Orders;
use yii\data\ActiveDataProvider;


class SearchOrders extends Orders
{
    // добавить 'amount_books' в rules()
    public function rules()
    {
        return [
            [[
                'order'
            ], 'safe']
        ];
    }
    public static function tableName()
    {
        return 'orders';
    }
    
    public function search($params)
    {


        $query = Orders::find()->from("{{%orders}}");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        $query->andFilterWhere(['like', 'order', $this->order]);


        return $dataProvider;
    }
}
