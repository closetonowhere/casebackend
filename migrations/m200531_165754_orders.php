<?php

use yii\db\Migration;

/**
 * Class m200531_165754_manual_payments
 */
class m200531_165754_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'operator_id' => $this->integer(),
            'order' => $this->string(400),
            'date_of_order' => $this->string(20),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200531_165754_manual_payments cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200531_165754_manual_payments cannot be reverted.\n";

        return false;
    }
    */
}
